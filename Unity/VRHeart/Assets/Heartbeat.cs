﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class Heartbeat : MonoBehaviour {

	private const string dllLocation = "CommArduino";

	[DllImport(dllLocation, CharSet = CharSet.Ansi)]
	private static extern int Init(string comPort);

	[DllImport(dllLocation, CharSet = CharSet.Ansi)]
	private static extern int HearbeatAsync();

	private bool Beat = false;
	private int Rate = -1;

	public string ComPort;

	public bool GetBeat()
	{
		if (Beat == true)
		{
			Beat = false; // Reset the beat
			return true;
		}
		else
		{
			return false;
		}
	}

	public int GetRate()
	{
		return Rate;
	}

	// Use this for initialization
	void Start () 
	{
		Init(ComPort);
	}
	
	// Update is called once per frame
	void Update () 
	{
		Rate = HearbeatAsync();
		if (Rate != -1)
		{
			Beat = true;
		}
	}
}
