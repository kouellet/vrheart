﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HeatbeatExample : MonoBehaviour {

	public Heartbeat heartbeat;
	public GameObject objectToChangeColor;
	public Text text;

	private int frame = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (frame != 0)
		{
			frame--;
			return;
		}

		if (heartbeat != null)
		{
			if (heartbeat.GetBeat())
			{
				frame = 5;
				objectToChangeColor.gameObject.GetComponent<Renderer>().material.color = Color.green;
				text.text = heartbeat.GetRate().ToString();
			}
			else
			{
				objectToChangeColor.gameObject.GetComponent<Renderer>().material.color = Color.red;
			}
		}
	}
}
