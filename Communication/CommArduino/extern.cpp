#define LIBRARY_API __declspec(dllexport)

#include <string>
#include <exception>


#include "SerialClass.h"

Serial* serial = nullptr;

static wchar_t* charToWChar(const char* text)
{
	size_t size = strlen(text) + 1;
	wchar_t* wa = new wchar_t[size];
	mbstowcs(wa, text, size);
	return wa;
}

extern "C"
{
	LIBRARY_API int Init(const char* com_port)
	{
		serial = new Serial(charToWChar(com_port));    // adjust as needed

		return 0;
	}

	LIBRARY_API int HearbeatAsync()
	{
		try
		{
			if (!serial->IsConnected())
				return -1;

			char incomingData[256] = "";			// don't forget to pre-allocate memory
													//printf("%s\n",incomingData);
			int dataLength = 256;
			int readResult = 0;

			readResult = serial->ReadData(incomingData, dataLength);
			if (readResult == -1)
			{
				return -1;
			}
			//printf("Bytes read: (-1 means no data available) %i\n", readResult);

			std::string heartbeat_string(incomingData);
			int heartbeat = std::stoi(heartbeat_string);

			return heartbeat;
		}
		catch (std::exception &e)
		{
			return -1;
		}
		catch (...)
		{
			return -1;
		}
	}
}